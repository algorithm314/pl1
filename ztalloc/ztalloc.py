import sys
from collections import deque

MAX_N = 1000008
bit = [-1] * MAX_N

def bit_update(pos,value):
	global bit
	pos += 2 # 0 is not valid value

	while pos < MAX_N:
		bit[pos] = max(bit[pos],value)
		pos += pos & (-pos)

def bit_get(pos):
	global bit
	pos += 2
	res = -1
	while pos > 0:
		res = max(bit[pos],res)
		pos -= pos & (-pos)

	return res

def seen_update(state):
	start, end = state
	bit_update(end,start)

def is_seen(state):
	start, end = state
	return bit_get(end) >= start

def solve(start, goal):
    q = deque([start])
    global bit
    bit = [-1] * MAX_N # init
    seen_update(start)
    prev = dict()
    def h(s): return (s[0]//2, s[1]//2)
    def t(s): return (s[0]*3+1, s[1]*3+1)
    while q:
        s = q.popleft()
        if goal[0] <= s[0] and goal[1] >= s[1]:
            if s == start:
                return 'EMPTY\n'
            answer = deque()
            while s != start:
                answer.appendleft(prev[s][1])
                s = prev[s][0]
            return ''.join(answer) + '\n'
        if s != (0,0) and not is_seen(h(s)):
            q.append(h(s))
            seen_update(h(s))
            prev[h(s)] = (s, 'h')
        if t(s)[0] <= 999999 and t(s)[1] <= 999999 and not is_seen(t(s)):
            q.append(t(s))
            seen_update(t(s))
            prev[t(s)] = (s, 't')
    else:
        return 'IMPOSSIBLE\n'

debugging = False
filename = sys.argv[1]
if(len(sys.argv) == 3):
    debug_file = sys.argv[2]
    with open(debug_file) as f:
        answers = f.readlines()
        debugging = True

with open(filename) as f:
    f.readline() # ignore first line
    for i, line in enumerate(f):
        l_in, r_in, l_out, r_out = map(int, line.split())
        answer = solve((l_in, r_in), (l_out, r_out))
        print(answer,end='')
