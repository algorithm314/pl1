import java.io.*;
import java.util.*;

public class SaveTheCat {
    private static int n, m;

    private static char[][] eisodos;

    private static Cell[][] grid;

    private static class Cell {
        public int r,c;
        public Cell prev = null;
        public char move = ' ';
        public int time_flooded = Integer.MAX_VALUE,
                time_cat = Integer.MAX_VALUE;
        public boolean water_vis = false, cat_vis = false;

        public Cell(int r, int c){
            this.r = r;
            this.c = c;
        }

        public boolean less(Cell other){
            return this.r < other.r || (this.r == other.r && this.c < other.c);
        }

        public Queue<Cell> getNeighbors(){
            Queue<Cell> neighbors = new ArrayDeque<>();
            if ( r != n-1 && eisodos[r+1][c] != 'X'){
                neighbors.add(grid[r+1][c]);
            }
            if ( c != 0 && eisodos[r][c-1] != 'X'){
                neighbors.add(grid[r][c-1]);
            }
            if ( c != m-1 && eisodos[r][c+1] != 'X'){
                neighbors.add(grid[r][c+1]);
            }
            if ( r != 0 && eisodos[r-1][c] != 'X'){
                neighbors.add(grid[r-1][c]);
            }
            return neighbors;
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(args[0]));
        List<String> lines = new ArrayList<String>();
        while (sc.hasNextLine()) {
            lines.add(sc.nextLine());
        }

        String[] inputStrings = lines.toArray(new String[0]);
        n = inputStrings.length;

        m = inputStrings[0].length();

        eisodos = new char[n][m];
        for(int i = 0; i < inputStrings.length; i++)
            eisodos[i] = inputStrings[i].toCharArray();
        solve();
    }

    public static void solve(){
        grid = new Cell[n][m];

        Queue<Cell> cat_q = new ArrayDeque<>();
        Queue<Cell> water_q = new ArrayDeque<>();
        Cell max_safe_square = null;

        for(int r = 0; r < n; r++){
            for(int c = 0; c < m; c++){
                char character = eisodos[r][c];
                grid[r][c] = new Cell(r, c);
                if(character == 'A'){
                   grid[r][c].time_cat = 0;
                   grid[r][c].cat_vis = true;
                   max_safe_square = grid[r][c];
                   cat_q.add(grid[r][c]);
                }
                else if(character == 'W'){
                    grid[r][c].time_flooded = 0;
                    water_q.add(grid[r][c]);
                }
            }
        }


        // Flood fill
        int time = 1;
        while (!water_q.isEmpty()){
            Cell elem = water_q.remove();
            time = elem.time_flooded;
            if (!elem.water_vis){
                elem.water_vis = true;
                for (Cell neighbor : elem.getNeighbors()){
                    neighbor.time_flooded = Math.min(
                            neighbor.time_flooded, time + 1);
                    water_q.add(neighbor);
                }
            }
        }

        // BFS for the cat
        int max_safe_time = 0;
        time = 0;
        while (!cat_q.isEmpty()){
            Cell elem = cat_q.remove();
            int safe_time = elem.time_flooded;
            if(elem.time_flooded != Integer.MAX_VALUE)
                safe_time = safe_time - 1;
            if(safe_time > max_safe_time ||
                    (safe_time == max_safe_time && elem.less(max_safe_square))){
                max_safe_time = safe_time;
                max_safe_square = elem;
            }

            time = elem.time_cat;
            for(Cell neighbor : elem.getNeighbors()){
                if (!neighbor.cat_vis && neighbor.time_flooded > time + 1){
                    neighbor.time_cat = time + 1;
                    neighbor.prev = elem;
                    neighbor.cat_vis = true;
                    if (neighbor.c > elem.c)
                        neighbor.move = 'R';
                    else if (neighbor.c < elem.c)
                        neighbor.move = 'L';
                    else if (neighbor.r < elem.r)
                        neighbor.move = 'U';
                    else
                        neighbor.move = 'D';
                    cat_q.add(neighbor);
                }
            }
        }

        String res;
        if(max_safe_time == Integer.MAX_VALUE)
            res = "infinity";
        else
            res = Integer.toString(max_safe_time);

        Cell square = max_safe_square;
        Deque<Character> moves = new ArrayDeque<>();

        while(square.prev != null){
            moves.addFirst(square.move);
            square = square.prev;
        }

        if(moves.isEmpty()) {
            System.out.println(res);
            System.out.println("stay");
        }
        else{
            System.out.println(res);
            for(char move : moves)
                System.out.print(move);
            System.out.print('\n');
        }
    }

}
