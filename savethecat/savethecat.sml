(* Προσοχή ο πίνακας είναι ανάποδα *)
open Array2

type cell = {r:int,c:int,pr:int,pc:int,dist:int};
type zeugos = {r:int,c:int,move:char};

val dest_r = ref 0;
val dest_c = ref 0;

val res:int ref = ref 0;

fun solve(input) =
	let
		val (rows,cols_tmp):int*int = Array2.dimensions(input);
		val cols:int = cols_tmp-1;
		val catQ = Queue.mkQueue():( int*int*int*int*int*char ) Queue.queue;
		val wQ = Queue.mkQueue():( int*int*int*int*int*char ) Queue.queue;
		val arrival_time = Array2.array(rows,cols,~1);
		val visited = Array2.array(rows,cols,0);
		val previous = Array2.array(rows,cols,(~1,~1,#"."));
		val res_r = ref 0;
		val res_c = ref 0;
		fun zero (x:int):int = 0;
		
		fun p(r,c) =
			if r = !dest_r andalso c = !dest_c then ()
			else
			let 
				val z = Array2.sub(previous,r,c);
			in
				p(#1 z, #2 z);
				print(Char.toString (#3 z))
			end;

		fun init(~1,_) = ()
		| init(r:int,c:int) =
		let
			val ch = Array2.sub(input,r,c);
			val next_r:int = if c = 0 then r-1 else r;
			val next_c:int = if c = 0 then cols-1 else c-1;
		in
						(*print(Int.toString r ^ Int.toString c ^ Char.toString ch);*)
			res_r := rows-1;
			if ch = #"A" then(
				Queue.enqueue(catQ,(r,c,~1,~1,0,#"n"));

				dest_r := r;
				dest_c := c;
				Array2.update(input,r,c,#".");
				init(next_r,next_c)
			)else if ch = #"W" then(
				Queue.enqueue(wQ,(r,c,~1,~1,0,#"n"));
				Array2.update(input,r,c,#".");
				init(next_r,next_c)
			)else
				init(next_r,next_c)
		end;
		
		fun up_pos(Q,r:int,c:int,V:int,H:int,dist:int,M) =
			if r+V >= 0 andalso c+H >= 0 andalso r+V < rows andalso c+H < cols then
				if	Array2.sub(input,r+V,c+H) = #"." andalso Array2.sub(visited,r+V,c+H) = 0 then
					Queue.enqueue(Q,(r+V,c+H,r,c,dist+1,M))
				else
					()
			else
				()
		
		fun bfs_w() =
		if Queue.isEmpty(wQ) then ()
		else
			let
				val front = Queue.dequeue(wQ);
				val r = #1 front;
				val c = #2 front;
				val dist = #5 front;
			in
				if Array2.sub(visited,r,c) = 1 then
					bfs_w()
				else(
					Array2.update(visited,r, c,1);
					up_pos(wQ,r,c,~1,0,dist,#"D");
					up_pos(wQ,r,c,0,~1,dist,#"L");
					up_pos(wQ,r,c,0,1,dist,#"R");
					up_pos(wQ,r,c,1,0,dist,#"U");
					
					Array2.update(arrival_time,r,c,dist);
					bfs_w()
				)
			end;
			
			(* TODO res, easy way reference*)
			
			fun bfs_c() =
			if Queue.isEmpty(catQ) then ()
			else
			let
				val front = Queue.dequeue(catQ);
				val r = #1 front;
				val c = #2 front;
				val pr = #3 front;
				val pc = #4 front;
				val dist = #5 front;
				val M = #6 front;
				val ar_time:int = Array2.sub(arrival_time,r,c);
				val at:int = ar_time-1;
			in
				if Array2.sub(visited,r,c) = 1 orelse dist >= ar_time then
					bfs_c()
				else(
					Array2.update(previous,r,c,(pr,pc,M));
					(*print("res =" ^ Int.toString (!res) ^ "\n");*)
					up_pos(catQ,r,c,~1,0,dist,#"D");
					up_pos(catQ,r,c,0,~1,dist,#"L");
					up_pos(catQ,r,c,0,1,dist,#"R");
					up_pos(catQ,r,c,1,0,dist,#"U");
					if at > !res orelse (at = !res andalso r < !res_r orelse( r = !res_r andalso c < !res_c)) then(
						res := at;
						res_r := r;
						res_c := c;
						bfs_c()
					)else bfs_c()
				)
			end;
	in
		init(rows-1,cols-1);
		bfs_w();
		Array2.modify RowMajor zero visited ;
		bfs_c();
		if !res = 0 then
			print("infinity\n")
		else(
			print(Int.toString (!res) ^ "\n")
		);
		if !dest_r = !res_r andalso !dest_c = !res_c then
			print("stay\n")
		else(
			p(!res_r,!res_c);
			print("\n")
		)
		(*print(Int.toString rows ^ Int.toString cols ^ "\n")*)
	end;

fun parse file =
	let
		val stream = TextIO.openIn file;
		fun to_char s = String.explode(Option.valOf s);
		fun read_input (stream,l) = 
			let
				val line = TextIO.inputLine stream
			in
				if line = NONE then 
					l
				else
					read_input(stream, to_char(line)::l)
			end;
	in
		Array2.fromList (read_input(stream,[]))
	end;

fun savethecat f = solve(parse(f));
