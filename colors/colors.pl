/*
 * A predicate that reads the input from File and returns it in
 * the last three arguments: N, K and C.
 * Example:
 *
 * ?- read_input('c1.txt', N, K, C).
 * N = 10,
 * K = 3,
 * C = [1, 3, 1, 3, 1, 3, 3, 2, 2|...].
 */

/*:- initialization main.

main :-
	current_prolog_flag(argv, [Testcase | _]),
	colors(Testcase, Answer),
	writeln(Answer),
	halt(0).
*/

read_input(File, N, K, C) :-
    open(File, read, Stream),
    read_line(Stream, [N, K]),
    read_line(Stream, C).

read_line(Stream, L) :-
    read_line_to_codes(Stream, Line),
    atom_codes(Atom, Line),
    atomic_list_concat(Atoms, ' ', Atom),
    maplist(atom_number, Atoms, L).

colors(File, Answer) :-
	read_input(File, N, K, Arr),
	empty_assoc(Hist),
	solve(Arr, Arr, Hist, 0, 0, 0, N, K, 1000000000, Answer),
	!.

	/* If Cnt == K we remove left elements */
solve([I|T1], L2, Hist, Left, Right, Cnt, N, Cnt, Sofar, Answer) :-
	(get_assoc(I, Hist, Value); Value is 0),
	NewValue is Value - 1,
	(NewValue =:= 0 -> NewCnt is Cnt - 1; NewCnt is Cnt),
	put_assoc(I, Hist, NewValue, NewHist),
	NewLeft is Left + 1,
	Temp1 is Right - Left,
	NewSofar is min(Sofar, Temp1),
	!,
	solve(T1, L2, NewHist, NewLeft, Right, NewCnt, N, Cnt, NewSofar, Answer).

	/* Else if Cnt != K but there are elements to the right we add them */
solve(L1, [J|T2], Hist, Left, Right, Cnt, N, K, Sofar, Answer) :-
	(get_assoc(J, Hist, Value); Value is 0),
	NewValue is Value + 1,
	(NewValue =:= 1 -> NewCnt is Cnt + 1; NewCnt is Cnt),
	put_assoc(J, Hist, NewValue, NewHist),
	NewRight is Right + 1,
	!,
	solve(L1, T2, NewHist, Left, NewRight, NewCnt, N, K, Sofar, Answer).

	/* Break condition */
solve(_, [], _, _, _, Cnt, N, K, Sofar, Answer) :-
	(Sofar =:= 1000000000 -> (Cnt =:= K -> Answer is N; Answer is 0)
	; Answer is Sofar).

