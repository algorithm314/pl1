#include <stdio.h>
#include <queue>
#include <string.h>
#define MAX_N 1024
using namespace std;

//seira D L R U
#define up_pos(Q,V,H,M)\
		if(grid[tmp.r + V][tmp.c + H] == '.' and visited[tmp.r + V][tmp.c + H] == 0)\
			Q.push((cell){tmp.r + V,tmp.c + H,tmp.r,tmp.c,tmp.dist+1,M});

#define update(Q)\
		visited[tmp.r][tmp.c] = 1;\
		up_pos(Q,1,0,'D');\
		up_pos(Q,0,-1,'L');\
		up_pos(Q,0,1,'R');\
		up_pos(Q,-1,0,'U');


char grid[MAX_N][MAX_N];
char visited[MAX_N][MAX_N];
int grammes,steiles;

struct cell{
	int r,c;
	int pr,pc,dist,move;
};

struct zeugos{
	int r,c,m;
};

queue<cell> gata,nero;
zeugos previous[MAX_N][MAX_N];
int arrival_time[MAX_N][MAX_N];

int dest_r,dest_c;

void print(int r,int c)
{
	if(r == dest_r and c == dest_c)
		return;
	print(previous[r][c].r,previous[r][c].c);
	putchar(previous[r][c].m);
}

int main(int argc, char **argv)
{
	freopen(argv[1],"r",stdin);
	int c,i,j;
	for(i = 1;;i++){
		for(j = 1; (c = getchar_unlocked()) != '\n';j++){
			if(c == EOF)
				goto end_loop;
			if(c == 'A'){
				gata.push((cell){i,j,-1,-1,0});
				c = '.';
				dest_r = i;
				dest_c = j;
			}else if (c == 'W'){
				nero.push((cell){i,j,-1,-1,0});
				c = '.';
			}
			grid[i][j] = c;
		}
		steiles = j-1;
	}
end_loop:
	grammes = i-1;
	for(i = 1;i <= steiles;i++)
		grid[0][i] = grid[grammes+1][i] = '#';
	for(i = 1;i <= grammes;i++)
		grid[i][0] = grid[i][steiles+1] = '#';
	cell tmp;
	memset(arrival_time,127,sizeof(arrival_time));
	while(!nero.empty()){
		tmp = nero.front();
		nero.pop();
		if(visited[tmp.r][tmp.c])
			continue;
		update(nero);
		arrival_time[tmp.r][tmp.c] = tmp.dist;
	}
	int res = 0,res_r = 0,res_c = 0;
	memset(visited,0,sizeof(visited));
	while(!gata.empty()){
		tmp = gata.front();
		gata.pop();
		if(visited[tmp.r][tmp.c])
			continue;
		if(tmp.dist >= arrival_time[tmp.r][tmp.c])
			continue;
		previous[tmp.r][tmp.c] = (zeugos){tmp.pr,tmp.pc,tmp.move};
		update(gata);
		int a_t = arrival_time[tmp.r][tmp.c]-1;
		if(a_t > res or 
		(a_t == res and (tmp.r < res_r or (tmp.r == res_r and tmp.c < res_c)))){
			res = a_t;
			res_r = tmp.r;
			res_c = tmp.c;
		}
		
	}
	if(res == 2139062142)
		printf("infinity\n");
	else
		printf("%d\n",res);

	if(dest_r == res_r and dest_c == res_c)
		printf("stay\n");
	else{
		print(res_r,res_c);
		putchar('\n');
	}
	
	return 0;
}
