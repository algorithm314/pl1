/*:- initialization main.

main :-
	current_prolog_flag(argv, [Testcase | _]),
	ztalloc(Testcase, Answers),
	maplist(writeln, Answers),
	halt(0).
*/

read_line(Stream, L) :-
    read_line_to_codes(Stream, Line),
    atom_codes(Atom, Line),
    atomic_list_concat(Atoms, ' ', Atom),
    maplist(atom_number, Atoms, L).

solve_lines(_, 0, Sofar, Sofar).

solve_lines(Stream, Q, Sofar, Answers) :-
    read_line(Stream, [L_in, R_in, L_out, R_out]),
	NewQ is Q-1,
	empty_queue(Queue),
	empty_assoc(Prev),
	empty_assoc(Seen),
	solve(L_in, R_in, L_out, R_out, L_in, R_in, Queue, Seen, Prev, Answer),
	append(Sofar, [Answer], NewSofar),
	!,
	solve_lines(Stream, NewQ, NewSofar, Answers).

ztalloc(File, Answers) :-
    open(File, read, Stream),
	read_line(Stream, Q),
	solve_lines(Stream, Q, [], Answers),!.

% Queue implementation: https://stackoverflow.com/questions/31918227/difference-lists-in-prolog-and-mutable-variables/31925828#31925828

%% empty_queue(-Queue)
% make an empty queue
empty_queue(queue(0, Q, Q)).

%% queue_head(?Queue, ?Head, ?Queue0)
% Queue, with Head removed, is Queue0
queue_head(queue(s(X), [H|Q], Q0), H, queue(X, Q, Q0)).

%% queue_last(+Queue0, +Last, -Queue)
% Queue0, with Last at its back, is Queue
queue_last(queue(X, Q, [L|Q0]), L, queue(s(X), Q, Q0)).

solve(L_in, R_in, L_out, R_out, S1, S2, Q, Seen, Prev, Answer) :-
	(S1 >= L_out, S2 =< R_out ->
		(S1 = L_in, S2 = R_in->
			Answer = 'EMPTY'
		;
			solution(L_in, R_in, S1, S2, Prev, [], Answer)
		)
	;
	H1 is div(S1,2),
	H2 is div(S2,2),
	((S1 =\= 0; S2 =\= 0), \+ get_assoc(H1/H2, Seen, _)->
		queue_last(Q, H1/H2, NewQ),
		put_assoc(H1/H2, Seen, 1, NewSeen),
		put_assoc(H1/H2, Prev, prev(S1/S2, h), NewPrev)
	;
		NewQ = Q,
		NewSeen = Seen,
		NewPrev = Prev
	),
	T1 is S1*3+1,
	T2 is S2*3+1,
	(T1 =< 999999, T2 =< 999999, \+ get_assoc(T1/T2, Seen, _)->
		queue_last(NewQ, T1/T2, NewNewQ),
		put_assoc(T1/T2, NewSeen, 1, NewNewSeen),
		put_assoc(T1/T2, NewPrev, prev(S1/S2, t), NewNewPrev)
	;
		NewNewQ = NewQ,
		NewNewSeen = NewSeen,
		NewNewPrev = NewPrev
	),

	(empty_queue(NewNewQ) -> 
		Answer = 'IMPOSSIBLE'
	;	
		queue_head(NewNewQ, Final1/Final2, FinalQ),
		!,
		solve(L_in, R_in, L_out, R_out, Final1, Final2, FinalQ, NewNewSeen, NewNewPrev, Answer)
	)
	).

solution(L_in, R_in, L_in, R_in, _, Sofar, Answer) :-
	atomic_list_concat(Sofar, '', Answer).

solution(L_in, R_in, S1, S2, Prev, Sofar, Answer) :-
	get_assoc(S1/S2, Prev, prev(Next1/Next2, Move)),
	NewSofar = [Move | Sofar],
	solution(L_in, R_in, Next1, Next2, Prev, NewSofar, Answer).

