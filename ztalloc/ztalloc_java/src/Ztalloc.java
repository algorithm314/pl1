import javax.sound.sampled.Line;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Ztalloc{
    private static class Tuple{
		public final int l,r;
		public Tuple prev;
		public char move;
		public Tuple(int l, int r){
			this.l = l;
			this.r = r;
			this.prev = null;
			this.move = ' ';
		}
		public Tuple(int l, int r, Tuple prev, char move){
			this.l = l;
			this.r = r;
			this.prev = prev;
			this.move = move;
		}

		@Override
		public boolean equals(Object obj) {
		    Tuple other = (Tuple) obj;
		    return other.l == this.l && other.r == this.r;
		}

		@Override
		public int hashCode() {
		    return Objects.hash(l, r); // TODO is this necessary?
		}
	}

	private static int[] bit;

	private static final int MAX_N = 1000008;

	public static void main(String[] args){
		bit = new int[MAX_N];
		try(BufferedReader br = new BufferedReader(new FileReader(args[0]))) {
			String line = br.readLine();
			int Q = Integer.parseInt(line);
			for(int i=0; i<Q; i++) {
				line = br.readLine();
				String[] arr = line.split(" ");
				int l_in = Integer.parseInt(arr[0]);
				int r_in = Integer.parseInt(arr[1]);
				int l_out = Integer.parseInt(arr[2]);
				int r_out = Integer.parseInt(arr[3]);
				String res = solve(l_in, r_in, l_out, r_out);
				System.out.print(res);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String solve(int l_in, int r_in, int l_out, int r_out) {
		for(int i=0; i < MAX_N; i++)
			bit[i] = -1;

	    Queue<Tuple> q = new LinkedList<>();
	    q.add(new Tuple(l_in, r_in));
		while(!q.isEmpty()){
			Tuple s = q.remove();

			if(l_out <= s.l && r_out >= s.r){
				if(s.l == l_in && s.r == r_in)
					return "EMPTY\n";
				StringBuilder sb = new StringBuilder();
				Stack<Character> answer = new Stack<>();
				while(!(s.l == l_in && s.r == r_in)){
					answer.add(s.move);
					s = s.prev;
				}
				while(!answer.isEmpty()){
					char c = answer.pop();
					sb.append(c);
				}
				return sb.toString() + "\n";
			}

			Tuple h = new Tuple(s.l/2, s.r/2, s, 'h');
			Tuple t = new Tuple(s.l*3+1, s.r*3+1, s, 't');

			if(!(s.l == 0 && s.r == 0) && !is_seen(h)){
				q.add(h);
				seen_update(h);
			}

			if(t.l <= 999999 && t.r <= 999999 && !is_seen(t)){
				q.add(t);
				seen_update(t);
			}
		}
		return "IMPOSSIBLE\n";
	}

	private static boolean is_seen(Tuple state){
		return bit_get(state.r) >= state.l;
	}

	private static void seen_update(Tuple state){
		bit_update(state.r, state.l);
	}

	private static void bit_update(int pos, int value){
		pos += 2;

		while(pos < MAX_N){
			bit[pos] = Math.max(bit[pos], value);
			pos += pos & (-pos);
		}
	}

	private static int bit_get(int pos){
		pos += 2;
		int res = -1;

		while(pos > 0){
			res = Math.max(bit[pos], res);
			pos -= pos & (-pos);
		}		

		return res;
	}
}
