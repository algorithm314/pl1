import sys 
from collections import deque

grid = None

class Cell:
    def __init__(self, r, c, prev = None, move = None,
            time_flooded = float("inf"), time_cat = float("inf"),
            water_vis = False, cat_vis = False):
        self.r = r
        self.c = c
        self.prev = prev
        self.move = move 
        self.time_flooded = time_flooded
        self.time_cat = time_cat
        self.water_vis = water_vis
        self.cat_vis = cat_vis

    def __lt__(self, other):
        return self.r < other.r or (self.r == other.r and self.c < other.c)

    def get_neighbors(self):
        neighbors = deque() 
        r = self.r
        c = self.c
        if r != n-1 and eisodos[r+1][c] != 'X':
            neighbors.append(grid[r+1][c])  # down
        if c != 0 and eisodos[r][c-1] != 'X':
            neighbors.append(grid[r][c-1])  # left
        if c != m-1 and eisodos[r][c+1] != 'X':
            neighbors.append(grid[r][c+1])  # right
        if r != 0 and eisodos[r-1][c] != 'X':
            neighbors.append(grid[r-1][c])  # up
        return neighbors


def solve(n, m, eisodos):
    global grid
    grid = [[None for c in range(m)] for r in range(n)]
    cat_q = deque()
    water_q = deque()
    for r in range(n):
        for c in range(m):
            char = eisodos[r][c]
            grid[r][c] = Cell(r, c)
            if char == 'A':
                grid[r][c].time_cat = 0
                grid[r][c].cat_vis = True
                max_safe_square = grid[r][c]
                cat_q.append(grid[r][c])
            elif char == 'W':
                grid[r][c].time_flooded = 0
                water_q.append(grid[r][c])

    # flood fill
    time = 1
    while water_q:
        elem = water_q.popleft()
        time = elem.time_flooded
        if not elem.water_vis: 
            elem.water_vis = True
            for neighbor in elem.get_neighbors():
                neighbor.time_flooded = min(neighbor.time_flooded, time + 1)
                water_q.append(neighbor)
    
    # bfs for the cat
    max_safe_time = 0
    time = 0
    while cat_q:
        elem = cat_q.popleft()
        safe_time = elem.time_flooded - 1
        if safe_time > max_safe_time or \
        (safe_time == max_safe_time and elem < max_safe_square):
            max_safe_time = safe_time
            max_safe_square = elem
        elif safe_time == max_safe_time and elem < max_safe_square:
            max_safe_square = elem

        time = elem.time_cat
        for neighbor in elem.get_neighbors():
            if not neighbor.cat_vis and neighbor.time_flooded > time + 1:
                neighbor.time_cat = time + 1
                neighbor.prev = elem
                neighbor.cat_vis = True
                if neighbor.c > elem.c:
                    neighbor.move = 'R'
                elif neighbor.c < elem.c:
                    neighbor.move = 'L'
                elif neighbor.r < elem.r:
                    neighbor.move = 'U'
                else:
                    neighbor.move = 'D'
                cat_q.append(neighbor)

    if max_safe_time == float("inf"):
        max_safe_time = "infinity"

    square = max_safe_square
    moves = deque()
    while square.prev:
        moves.appendleft(square.move)
        square = square.prev

    if not moves:
        return str(max_safe_time) + "\nstay\n"
    else:
        return str(max_safe_time) + '\n' + ''.join(moves) + '\n'

debugging = False
filename = sys.argv[1]
if len(sys.argv) == 3:
    debug_file = sys.argv[2]
    with open(debug_file) as f:
        answer = f.read()
        debugging = True

with open(filename) as f:
    eisodos = [x.strip() for x in f.readlines()]
    # eisodos[row][column]
    eisodos = list(map(list,eisodos))
    # n = number of rows
    n = len(eisodos)
    # m = number of columns
    m = len(eisodos[0])
    solution = solve(n, m, eisodos)
    if debugging:
        if solution == answer:
            print(solution)
        else:
            print('WRONG\t' , solution)
            print('CORRECT\t', answer)
            sys.exit(1)
    else:
        print(solution)
