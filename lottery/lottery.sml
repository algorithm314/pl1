(* This solution is based on hashing insted of a trie *)

val res_count = ref 0;
val res_sum = ref 0;
val M = IntInf.toLarge 1000000007;

val ht : (Word.word, int) HashTable.hash_table =
    HashTable.mkTable (fn x => x, op=) (42, Fail "not found")

fun hash([#"\n"]):Word.word = Word.fromInt(0)
|	hash(head::tail) = ((Word.fromInt(Char.ord(head))-Word.fromInt(Char.ord(#"0"))+Word.fromInt(1))+Word.fromInt(11)*hash(tail)) mod Word.fromInt 1000000007;

fun insert([#"\n"]) = ()
|	insert(head::tail) = 
	let
		val h = hash(head::tail);
		val x = HashTable.find ht h;
	in	
		if Option.isSome x  then(
			HashTable.remove ht h;
			HashTable.insert ht (h,(Option.valOf(x)+1));
			insert(tail)
		)else(
			HashTable.insert ht (h,1);
			insert(tail)
		)
	end;

fun lookup(l:char list,K:int) =
	let
		fun in_lookup([],depth:int,found:int,s:IntInf.int) = s
		| in_lookup(head::tail,depth:int,found:int,s:IntInf.int) =
			let
				val h = hash(head::tail);
				val x = HashTable.find ht h;
			in
				if Option.isSome x  then(
					let					
						val v = Option.valOf(x);
						val sum = (((IntInf.pow(2,depth)-IntInf.toLarge (1)+M) mod M)*IntInf.fromInt(v-found) + s) mod M;
					in
						res_count := (!res_count + (v-found)) mod 1000000007;
						in_lookup(tail,depth-1,v,sum)
					end
				)else(
					in_lookup(tail,depth-1,found,s)
				)
			end;
	in
		res_count := 0;
		res_sum := 0;
		let
			val tmp = in_lookup(l,K,0,0);
		in
			print(Int.toString (!res_count) ^ " " ^ IntInf.toString (tmp) ^ "\n")
		end
	end;




(***************************************************************************
  Project     : Programming Languages 1 - Assignment 1 - Exercise 1
  Author(s)   : <Author name> (<author mail>)
  Date        : April 08, 2013
  Description : Teh S3cret Pl4n
  -----------
  School of ECE, National Technical University of Athens.
*)

(* Input parse code by Stavros Aronis, modified by Nick Korasidis. *)
fun parse file =
    let
	(* A function to read an integer from specified input. *)
		fun readInt input = 
			Option.valOf (TextIO.scanStream (Int.scan StringCvt.DEC) input)

	(* Open input file. *)
		val inStream = TextIO.openIn file

		(* Read an integer (number of countries) and consume newline. *)
		val K = readInt inStream
		val N = readInt inStream
		val Q = readInt inStream

		val _ = TextIO.inputLine inStream
        (* A function to read N integers from the open file. *)
	fun readInts(0:int, acc:string list) = rev acc (* Replace with 'rev acc' for proper order. *)
	  | readInts(i:int, acc:string list) = readInts (i - 1,Option.valOf(TextIO.inputLine inStream) :: acc)
    in
   	(readInts (N, []),readInts( Q ,[]), K)
    end;

fun solve( N:string list, Q:string list, K:int) = (
	List.app (fn x => insert(explode x)) N;
	List.app (fn x => lookup(explode x,K)) Q
);

fun lottery file = solve(parse(file));
(*val _ =
    let
        val a = colors (hd (CommandLine.arguments()))
    in
        print(Int.toString a ^ "\n") 
    end
*)
